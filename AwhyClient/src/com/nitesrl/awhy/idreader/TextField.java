package com.nitesrl.awhy.idreader;

/**
 * Class that implements the Field interface and contains the methods to describe a text field
 * @see Field
 * @see FieldBox
 * @see Interpretation
 * @author <a href="http://caniodica.com" target="blank">caniodica</a> for <a href="http://nitesrl.com" target="blank">NITe srl</a> (<a href="mailto:caniodica@nitesrl.com">caniodica[at]nitesrl.com</a>)
 * @author <a href="http://nitesrl.com" target="blank">NITe srl</a>
 * @version 0.2
 */
public class TextField{
	
	private FieldType field_type;
	private String interpreation = "";
	private String label = "";
	private boolean printedText = true;
	
	public TextField() {
		this.field_type = FieldType.UNKNOWN;
	}
	
	public TextField(FieldType type) {
		this.field_type = type;
	}

	public void setFieldType(FieldType fieldType) {
		this.field_type = fieldType;
		
	}

	public FieldType getFieldType() {
		return this.field_type;
	}

	public boolean isTypedIn() {
		return this.printedText;
	}

	public void setTypedIn() {
		this.printedText = true;
		
	}
	
	public void setHandwritten() {
		this.printedText = false;
		
	}
	public void setTypedIn(boolean typedIn) {
		this.printedText = typedIn;
		
	}
	
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getTranscription() {
		return this.interpreation;
	}
	
	public void setTranscription(String text) {
		this.interpreation = text;
	}
	
	

}
