
package com.nitesrl.awhy.idreader.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IDReaderWS", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IDReaderWS {


    /**
     * 
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "run", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.Run")
    @ResponseWrapper(localName = "runResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.RunResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/runRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/runResponse")
    public boolean run();

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "initialize", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.Initialize")
    @ResponseWrapper(localName = "initializeResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.InitializeResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/initializeRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/initializeResponse")
    public boolean initialize(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        int arg1);

    /**
     * 
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getID", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetID")
    @ResponseWrapper(localName = "getIDResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetIDResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getIDRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getIDResponse")
    public int getID();

    /**
     * 
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "cancel", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.Cancel")
    @ResponseWrapper(localName = "cancelResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.CancelResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/cancelRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/cancelResponse")
    public boolean cancel();

    /**
     * 
     * @return
     *     returns long
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getExecutionTime", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetExecutionTime")
    @ResponseWrapper(localName = "getExecutionTimeResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetExecutionTimeResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getExecutionTimeRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getExecutionTimeResponse")
    public long getExecutionTime();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getJSONString", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetJSONString")
    @ResponseWrapper(localName = "getJSONStringResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetJSONStringResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getJSONStringRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getJSONStringResponse")
    public String getJSONString();

    /**
     * 
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "setTimeOut", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.SetTimeOut")
    @ResponseWrapper(localName = "setTimeOutResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.SetTimeOutResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/setTimeOutRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/setTimeOutResponse")
    public boolean setTimeOut(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

    /**
     * 
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "isDone", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.IsDone")
    @ResponseWrapper(localName = "isDoneResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.IsDoneResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/isDoneRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/isDoneResponse")
    public boolean isDone();

    /**
     * 
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getProgress", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetProgress")
    @ResponseWrapper(localName = "getProgressResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.GetProgressResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getProgressRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/getProgressResponse")
    public int getProgress();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "ping", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.Ping")
    @ResponseWrapper(localName = "pingResponse", targetNamespace = "http://ws.idreader.awhy.nitesrl.com/", className = "com.nitesrl.awhy.idreader.ws.PingResponse")
    @Action(input = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/pingRequest", output = "http://ws.idreader.awhy.nitesrl.com/IDReaderWS/pingResponse")
    public String ping();

}
