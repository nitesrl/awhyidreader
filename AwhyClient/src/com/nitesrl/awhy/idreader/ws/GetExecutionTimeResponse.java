
package com.nitesrl.awhy.idreader.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getExecutionTimeResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getExecutionTimeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getExecutionTimeResponse", propOrder = {
    "_return"
})
public class GetExecutionTimeResponse {

    @XmlElement(name = "return")
    protected long _return;

    /**
     * Recupera il valore della proprietÓ return.
     * 
     */
    public long getReturn() {
        return _return;
    }

    /**
     * Imposta il valore della proprietÓ return.
     * 
     */
    public void setReturn(long value) {
        this._return = value;
    }

}
