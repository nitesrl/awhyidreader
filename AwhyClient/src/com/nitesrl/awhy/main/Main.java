package com.nitesrl.awhy.main;

import org.json.simple.JSONObject;

import com.nitesrl.awhy.idreader.IDReader;

public class Main {
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		
		
		// Crea un oggetto IDReader con un URL
		IDReader idreader = new IDReader("http://caniodica.ddns.net:33400/");
		
		// Controlla se il server � up
		System.out.print("Ping... ");
		System.out.println(idreader.ping());
		
		// Carica un immagine e il tipo di documento da leggere
		idreader.load("img\\DOC001_ts_back.png", IDReader.TYPE_TesseraSanitaria);
		
		// Esegui
		idreader.run();
		
		// Attendi la fine del processo
		while (!idreader.isDone())
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Scrivi il file json
		idreader.writeJSON("img\\out");
		
		// Oppure preleva l'oggetto JSON per farci qualcosa
		JSONObject jObj = idreader.getJSONObject();
		
		
		
		
	}

}
