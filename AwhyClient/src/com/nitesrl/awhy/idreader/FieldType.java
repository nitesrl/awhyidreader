package com.nitesrl.awhy.idreader;

/**
 * Enumerated type with the possible field types
 * @author <a href="http://caniodica.com" target="blank">caniodica</a> for <a href="http://nitesrl.com" target="blank">NITe srl</a> (<a href="mailto:caniodica@nitesrl.com">caniodica[at]nitesrl.com</a>)
 * @author <a href="http://nitesrl.com" target="blank">NITe srl</a>
 * @version 0.2
 */

public enum FieldType {

	SURNAME,
	NAME,
	BIRTH_DATE,
	CITY_OF_BIRTH,
	DOC_NUMBER,
	DOC_DATE,
	EXPIRATION_DATE,
	FISCAL_CODE,
	UNKNOWN	
}
