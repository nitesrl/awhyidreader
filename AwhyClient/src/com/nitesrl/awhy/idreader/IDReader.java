package com.nitesrl.awhy.idreader;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.nitesrl.awhy.idreader.ws.IDReaderWS;
import com.nitesrl.awhy.idreader.ws.IDReaderWSService;

public class IDReader {
	
	public final static int TYPE_Patente 		   = 0;
	public final static int TYPE_TesseraSanitaria  = 1;
	public final static int TYPE_CartaIdentita_OUT = 2;
	public final static int TYPE_CartaIdentita_IN  = 3;
	public final static int TYPE_CartaIdentita_E   = 4;
	
	IDReaderWS ws;
	public static String url = "http://localhost:8080/";
	
	
	/**
	 * Default constructor
	 */
	public IDReader() {
		IDReaderWSService service = new IDReaderWSService();
		this.ws = service.getIDReaderWSPort();
	}
	
	public IDReader(String url) {
		IDReader.url = url;
		IDReaderWSService service = new IDReaderWSService();
		this.ws = service.getIDReaderWSPort();
	}
	
	public IDReader(IDReaderWS ws) {
		this.ws = ws;
	}
	
	
	/**
	 * Load an image
	 * @param pathInput
	 * @param type
	 * @return
	 */
	public boolean load (String pathInput, int type) {
		try {
			BufferedImage img = ImageIO.read(new File(pathInput));
			return load(img, type);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Load a BufferedImage
	 * @param img
	 * @param type
	 * @return
	 */
	public boolean load(BufferedImage img, int type) {
		if (type < 0 || type > 1) {
			System.err.println("Unrecognized Type");
			return false;
		}
		String base64img = imgToBase64(img);
		if (!base64img.equals(null))
			return this.ws.initialize(base64img, type);
		return false;
 	}
	
	/**
	 * Run the process
	 * @return
	 */
	public boolean run() {
		return this.ws.run();
	}
	
	/**
	 * Cancel the process
	 * @return
	 */
	public boolean cancel() {
		return this.ws.cancel();
	}
	
	/**
	 * Check if the process is done
	 * @return
	 */
	public boolean isDone() {
		return this.ws.isDone();
	}
	
	/**
	 * Get the progress of the process (0-100)
	 * @return
	 */
	public int getProgress() {
		return this.ws.getProgress();
	}
	
	/**
	 * Set a timeout for the process (in seconds)
	 * @param seconds
	 * @return
	 */
	public boolean setTimeout(int seconds) {
		return this.ws.setTimeOut(seconds);
	}
	
	/**
	 * Get the execution time in ms
	 * @return
	 */
	public long getExecutionTime() {
		return this.ws.getExecutionTime();
	}
	
	/**
	 * Get the json Object
	 * @return
	 */
	public JSONObject getJSONObject() {
		String jsonString = this.ws.getJSONString();
		if (jsonString.equals(null))
			return null;
		try {
			Object o = new JSONParser().parse(jsonString);
			return (JSONObject) o;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Write the json Object on file
	 * @param pathOut
	 */
	public void writeJSON(String pathOut) {
		String jsonString = this.ws.getJSONString();
		if (jsonString.equals(null))
			return;
		if (!pathOut.endsWith(".json"))
			pathOut+=".json";
		File out = new File(pathOut);
		try (FileWriter fw = new FileWriter(out)) {
			fw.write(jsonString);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Check if the ws is up
	 * @return
	 */
	public String ping() {
		return ws.ping();
	}
	
	/**
	 * Convert an image in a Base64 string
	 * @param image
	 * @return
	 */
	private String imgToBase64(BufferedImage image) {
		try {
	    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			return Base64.getEncoder().encodeToString(imageInByte);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	

}
