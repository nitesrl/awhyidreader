
package com.nitesrl.awhy.idreader.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.nitesrl.awhy.idreader.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetJSONStringResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getJSONStringResponse");
    private final static QName _PingResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "pingResponse");
    private final static QName _GetExecutionTime_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getExecutionTime");
    private final static QName _Initialize_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "initialize");
    private final static QName _GetJSONString_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getJSONString");
    private final static QName _GetExecutionTimeResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getExecutionTimeResponse");
    private final static QName _SetTimeOut_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "setTimeOut");
    private final static QName _SetTimeOutResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "setTimeOutResponse");
    private final static QName _InitializeResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "initializeResponse");
    private final static QName _GetIDResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getIDResponse");
    private final static QName _GetProgress_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getProgress");
    private final static QName _IsDone_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "isDone");
    private final static QName _RunResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "runResponse");
    private final static QName _GetID_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getID");
    private final static QName _Run_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "run");
    private final static QName _Ping_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "ping");
    private final static QName _Cancel_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "cancel");
    private final static QName _GetProgressResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "getProgressResponse");
    private final static QName _CancelResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "cancelResponse");
    private final static QName _IsDoneResponse_QNAME = new QName("http://ws.idreader.awhy.nitesrl.com/", "isDoneResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.nitesrl.awhy.idreader.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cancel }
     * 
     */
    public Cancel createCancel() {
        return new Cancel();
    }

    /**
     * Create an instance of {@link GetProgressResponse }
     * 
     */
    public GetProgressResponse createGetProgressResponse() {
        return new GetProgressResponse();
    }

    /**
     * Create an instance of {@link CancelResponse }
     * 
     */
    public CancelResponse createCancelResponse() {
        return new CancelResponse();
    }

    /**
     * Create an instance of {@link IsDoneResponse }
     * 
     */
    public IsDoneResponse createIsDoneResponse() {
        return new IsDoneResponse();
    }

    /**
     * Create an instance of {@link Ping }
     * 
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link GetID }
     * 
     */
    public GetID createGetID() {
        return new GetID();
    }

    /**
     * Create an instance of {@link Run }
     * 
     */
    public Run createRun() {
        return new Run();
    }

    /**
     * Create an instance of {@link GetIDResponse }
     * 
     */
    public GetIDResponse createGetIDResponse() {
        return new GetIDResponse();
    }

    /**
     * Create an instance of {@link GetProgress }
     * 
     */
    public GetProgress createGetProgress() {
        return new GetProgress();
    }

    /**
     * Create an instance of {@link IsDone }
     * 
     */
    public IsDone createIsDone() {
        return new IsDone();
    }

    /**
     * Create an instance of {@link RunResponse }
     * 
     */
    public RunResponse createRunResponse() {
        return new RunResponse();
    }

    /**
     * Create an instance of {@link InitializeResponse }
     * 
     */
    public InitializeResponse createInitializeResponse() {
        return new InitializeResponse();
    }

    /**
     * Create an instance of {@link GetExecutionTimeResponse }
     * 
     */
    public GetExecutionTimeResponse createGetExecutionTimeResponse() {
        return new GetExecutionTimeResponse();
    }

    /**
     * Create an instance of {@link SetTimeOut }
     * 
     */
    public SetTimeOut createSetTimeOut() {
        return new SetTimeOut();
    }

    /**
     * Create an instance of {@link SetTimeOutResponse }
     * 
     */
    public SetTimeOutResponse createSetTimeOutResponse() {
        return new SetTimeOutResponse();
    }

    /**
     * Create an instance of {@link GetJSONString }
     * 
     */
    public GetJSONString createGetJSONString() {
        return new GetJSONString();
    }

    /**
     * Create an instance of {@link GetExecutionTime }
     * 
     */
    public GetExecutionTime createGetExecutionTime() {
        return new GetExecutionTime();
    }

    /**
     * Create an instance of {@link Initialize }
     * 
     */
    public Initialize createInitialize() {
        return new Initialize();
    }

    /**
     * Create an instance of {@link GetJSONStringResponse }
     * 
     */
    public GetJSONStringResponse createGetJSONStringResponse() {
        return new GetJSONStringResponse();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJSONStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getJSONStringResponse")
    public JAXBElement<GetJSONStringResponse> createGetJSONStringResponse(GetJSONStringResponse value) {
        return new JAXBElement<GetJSONStringResponse>(_GetJSONStringResponse_QNAME, GetJSONStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "pingResponse")
    public JAXBElement<PingResponse> createPingResponse(PingResponse value) {
        return new JAXBElement<PingResponse>(_PingResponse_QNAME, PingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExecutionTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getExecutionTime")
    public JAXBElement<GetExecutionTime> createGetExecutionTime(GetExecutionTime value) {
        return new JAXBElement<GetExecutionTime>(_GetExecutionTime_QNAME, GetExecutionTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Initialize }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "initialize")
    public JAXBElement<Initialize> createInitialize(Initialize value) {
        return new JAXBElement<Initialize>(_Initialize_QNAME, Initialize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJSONString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getJSONString")
    public JAXBElement<GetJSONString> createGetJSONString(GetJSONString value) {
        return new JAXBElement<GetJSONString>(_GetJSONString_QNAME, GetJSONString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExecutionTimeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getExecutionTimeResponse")
    public JAXBElement<GetExecutionTimeResponse> createGetExecutionTimeResponse(GetExecutionTimeResponse value) {
        return new JAXBElement<GetExecutionTimeResponse>(_GetExecutionTimeResponse_QNAME, GetExecutionTimeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTimeOut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "setTimeOut")
    public JAXBElement<SetTimeOut> createSetTimeOut(SetTimeOut value) {
        return new JAXBElement<SetTimeOut>(_SetTimeOut_QNAME, SetTimeOut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTimeOutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "setTimeOutResponse")
    public JAXBElement<SetTimeOutResponse> createSetTimeOutResponse(SetTimeOutResponse value) {
        return new JAXBElement<SetTimeOutResponse>(_SetTimeOutResponse_QNAME, SetTimeOutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitializeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "initializeResponse")
    public JAXBElement<InitializeResponse> createInitializeResponse(InitializeResponse value) {
        return new JAXBElement<InitializeResponse>(_InitializeResponse_QNAME, InitializeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getIDResponse")
    public JAXBElement<GetIDResponse> createGetIDResponse(GetIDResponse value) {
        return new JAXBElement<GetIDResponse>(_GetIDResponse_QNAME, GetIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProgress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getProgress")
    public JAXBElement<GetProgress> createGetProgress(GetProgress value) {
        return new JAXBElement<GetProgress>(_GetProgress_QNAME, GetProgress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsDone }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "isDone")
    public JAXBElement<IsDone> createIsDone(IsDone value) {
        return new JAXBElement<IsDone>(_IsDone_QNAME, IsDone.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RunResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "runResponse")
    public JAXBElement<RunResponse> createRunResponse(RunResponse value) {
        return new JAXBElement<RunResponse>(_RunResponse_QNAME, RunResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getID")
    public JAXBElement<GetID> createGetID(GetID value) {
        return new JAXBElement<GetID>(_GetID_QNAME, GetID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Run }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "run")
    public JAXBElement<Run> createRun(Run value) {
        return new JAXBElement<Run>(_Run_QNAME, Run.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ping }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "ping")
    public JAXBElement<Ping> createPing(Ping value) {
        return new JAXBElement<Ping>(_Ping_QNAME, Ping.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cancel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "cancel")
    public JAXBElement<Cancel> createCancel(Cancel value) {
        return new JAXBElement<Cancel>(_Cancel_QNAME, Cancel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProgressResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "getProgressResponse")
    public JAXBElement<GetProgressResponse> createGetProgressResponse(GetProgressResponse value) {
        return new JAXBElement<GetProgressResponse>(_GetProgressResponse_QNAME, GetProgressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "cancelResponse")
    public JAXBElement<CancelResponse> createCancelResponse(CancelResponse value) {
        return new JAXBElement<CancelResponse>(_CancelResponse_QNAME, CancelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsDoneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.idreader.awhy.nitesrl.com/", name = "isDoneResponse")
    public JAXBElement<IsDoneResponse> createIsDoneResponse(IsDoneResponse value) {
        return new JAXBElement<IsDoneResponse>(_IsDoneResponse_QNAME, IsDoneResponse.class, null, value);
    }

}
